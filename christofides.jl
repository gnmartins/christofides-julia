#!/usr/bin/env julia
include("graph.jl")
include("io.jl")
include("mst.jl")
include("matching.jl")
include("paths.jl")

using ArgParse

s = ArgParseSettings("TSP Solver using Christofides Algorithm")
@add_arg_table s begin
    "--exactMatching", "-e"
        action = :store_true
        help = "uses exact perfect matching instead of the heuristic default"
    "--elapsedTime", "-t"
        action = :store_true
        help = "outputs total elapsed time"
end
parsed_args = parse_args(s)
exact_matching = parsed_args["exactMatching"]
output_time = parsed_args["elapsedTime"]

# 0. Read instance
g = read_TSPLIB()

tic()

# 1. Find minimum spanning tree 
mst = prim_mst(g)
# 1.1. Add edges to graph
for i = 2 : length(mst)
    add_edge!(g, i, mst[i])
end

# 2. Find minimum-weight perfect matching 
# 2.1. O is the set of vertices with odd degree
degrees = degree(g)
O = Vector{Int64}()
for i = 1 : nv(g)
    if degrees[i] % 2 == 1
        push!(O, i)
    end
end

# 2.2. Find minimum-cost perfect matching on O
mates = []
if !exact_matching
    mates = greedy_matching(g, O)
else
    mates = blossom_matching(g, O)
end
added = Dict{Int64, Bool}()
for (u, v) in mates
    if !haskey(added, u) && !haskey(added, v)
        add_edge!(g, u, v)
        added[u] = true
        added[v] = true
    end
end
added = []

# 3. Find Eulerian circuit
circuit = eulerian_circuit!(g)

# 4. Create Hamiltonian path from Eulerian circuit
path, cost = eulerian_to_hamiltonian(g, circuit)

println(cost)
output_time && println("\telapsed time: $(toq()) seconds")

