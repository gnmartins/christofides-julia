function greedy_matching(g :: TSPGraph, O :: Vector{Int64})
    mates = Dict{Int64, Int64}()
    odds = []
    for i in O
        mates[i] = -1
        insert!(odds, 1, i)
    end

    while length(odds) > 0
        v = pop!(odds)
        l = typemax(Float64)
        closest = -1
        index = -1
        for i = 1 : length(odds)
            u = odds[i]
            dist = get_dist(g, u, v)
            if dist < l 
                l = dist
                closest = u
                index = i
            end
        end
        mates[v] = closest
        mates[closest] = v
        deleteat!(odds, index)
    end

    return mates
end

function blossom_matching(g :: TSPGraph, O :: Vector{Int64})
    mates = Dict{Int64, Int64}()
    blossom_input = "blossom_input.tsp"
    blossom_output = "blossom_output.out"
    try 
        # 1. create TSPLIB file based on vertices with odd degree in `g`
        io = open(blossom_input, "w+")

        n = length(O)
        mapping = Dict{Int64, Int64}()
        vertices_count = 0
        println(io, "DIMENSION: $n")
        println(io, "EDGE_WEIGHT_TYPE: EUC_2D")
        println(io, "NODE_COORD_SECTION") 
        for v in O
            vertices_count += 1
            x, y = get_pos(g, v)
            mapping[vertices_count] = v
            vertex_line = @sprintf("%d %f %f", vertices_count, x, y)
            println(io, vertex_line)
        end
        println(io, "EOF") 
        close(io)

        # 2. run blossom5 
        readstring(`./blossom5 -g $blossom_input -V -w $blossom_output`);

        # 3. parse blossom5 output
        io = open(blossom_output)
        lines = readlines(io)
        for line in lines
            tokens = split(strip(line))
            length(tokens) < 3 && continue
            u, v = parse(Int64, tokens[1])+1, parse(Int64, tokens[2])+1
            mates[mapping[u]] = mapping[v]
        end
        close(io)

        # 4. cleanup
        readstring(`rm -f $blossom_input $blossom_output`)
        return mates
    catch
        readstring(`rm -f $blossom_input $blossom_output`)
        println("Failed to run exact matching. Make sure `blossom5` is in the directory" *
            " and this program has permissions to write in the current folder.")
    end
end