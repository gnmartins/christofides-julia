function eulerian_circuit!(g :: TSPGraph)
    stack = Vector{Int64}()
    circuit = Vector{Int64}()
    degrees = degree(g)

    curr = 1
    #for i = 1 : nv(g)
    #    if degrees[i] % 2 == 1
    #        curr = i
    #        break
    #    end
    #end

    while true
        neighbors = get_neighbors(g, curr)
        length(stack) != 0 || length(neighbors) > 0 || break

        if length(neighbors) == 0
            push!(circuit, curr)
            curr = pop!(stack)
        else
            push!(stack, curr)
            neighbor = first(neighbors)
            rem_edge!(g, curr, neighbor)
            curr = neighbor
        end
    end

    push!(circuit, curr)
    return circuit
end

function eulerian_to_hamiltonian(g :: TSPGraph, euler :: Vector{Int64})
    path = Vector{Int64}()
    cost = 0
    visited = fill(false, nv(g))
    
    for i = 1 : length(euler)
        v = euler[i]
        if !visited[v]
            visited[v] = true
            if i > 1
                cost += get_dist(g, last(path), v)
            end
            push!(path, v)
        end
    end
    cost += get_dist(g, last(path), first(path))
    push!(path, first(path))
    return path, cost
end