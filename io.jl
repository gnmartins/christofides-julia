function read_TSPLIB()
    n = 0
    while true
        line = readline(STDIN)
        splitted = split(replace(line, " ", ""), ":")
        if splitted[1] == "DIMENSION"
            n = parse(Int64, splitted[2])
            break
        end
    end

    while true
        line = readline(STDIN)
        line[1:18] == "NODE_COORD_SECTION" && break
    end

    g = TSPGraph(n)
    readnodes = 0
    while readnodes < n
        line = split(readline(STDIN))
        v, x, y = parse(Int64, line[1]), parse(Float64, line[2]), parse(Float64, line[3])
        set_pos!(g, v, (x, y))
        readnodes += 1
    end

    return g
end