using Base.Collections

function prim_mst(g :: TSPGraph)

    known = fill(false, nv(g))
    cost = fill(typemax(Float64), nv(g))
    path = fill(-1, nv(g))

    Q = PriorityQueue{Int64, Float64, Base.Order.Ordering}()

    cost[1] = 0
    enqueue!(Q, 1, cost[1])
    while length(Q) != 0
        u = dequeue!(Q)
        known[u] = true
        for v = 1 : nv(g)
            known[v] && continue
            dist = get_dist(g, u, v)
            if cost[v] == typemax(Float64)
                cost[v] = dist
                path[v] = u
                enqueue!(Q, v, cost[v])
            elseif cost[v] > dist
                cost[v] = dist
                path[v] = u
                Q[v] = cost[v]
            end
        end
    end

    return path
end