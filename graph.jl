using LightGraphs

type TSPGraph 
    g :: Graph
    npos :: Vector{Tuple{Float64, Float64}}
    dupl_edges :: Dict{Tuple{Int64, Int64}, Int64}
    TSPGraph(n :: Int64) = new(Graph(n), [(0,0) for i = 1:n], Dict{Tuple{Int64, Int64}, Int64}())
end

"""
    nv
        Returns the number of vertices on `g`
"""
nv(g :: TSPGraph) = LightGraphs.nv(g.g)

"""
    ne
        Returns the number of edges on `g`
"""
ne(g :: TSPGraph) = LightGraphs.ne(g.g)

"""
    get_pos
        Returns the 2d position of the vertex `v`
"""
get_pos(g :: TSPGraph, v :: Int64) = g.npos[v]

"""
    set_pos
        Sets the 2d position of the vertex `v`
"""
function set_pos!(g :: TSPGraph, v :: Int64, pos :: Tuple{Float64,Float64})
    g.npos[v] = pos
    return g
end

"""
    get_dist
        Returns the distance between the vertices `u` and `v`, i.e.
        the weight of the edge `(u, v)`
"""
get_dist(g :: TSPGraph, u :: Int64, v :: Int64) = 
    sqrt((g.npos[u][1] -  g.npos[v][1]) ^ 2 +
         (g.npos[u][2] -  g.npos[v][2]) ^ 2)

"""
    degree
        Returns the degrees of all vertices on `g`
"""
degree(g :: TSPGraph) = LightGraphs.degree(g.g)

"""
    get_neighbors
        Returns the neighbors of the vertex `v` on `g`
"""
get_neighbors(g :: TSPGraph, v :: Int64) = LightGraphs.out_neighbors(g.g, v)

"""
    edges
        Returns iterator over edges of `g`
"""
edges(g :: TSPGraph) = LightGraphs.edges(g.g)

"""
    add_edge!
        Adds edge `(u, v)` to `g`

    This function allows adding duplicate edges with the same weight
"""
function add_edge!(g :: TSPGraph, u :: Int64, v :: Int64)
    if has_edge(g.g, u => v) 
        if haskey(g.dupl_edges, (u, v))
            g.dupl_edges[(u, v)] += 1
            g.dupl_edges[(v, u)] += 1
        else
            g.dupl_edges[(u, v)] = 1
            g.dupl_edges[(v, u)] = 1
        end
    else
        LightGraphs.add_edge!(g.g, u => v)
    end
end

"""
    rem_edge!
        Removes edge `(u, v)` of `g`
"""
function rem_edge!(g :: TSPGraph, u :: Int64, v :: Int64)
    if haskey(g.dupl_edges, (u, v)) || haskey(g.dupl_edges, (v, u))
        if g.dupl_edges[(u, v)] > 0
            g.dupl_edges[(u, v)] -= 1
            g.dupl_edges[(v, u)] -= 1
        else
            LightGraphs.rem_edge!(g.g, u => v)
        end
    else
        LightGraphs.rem_edge!(g.g, u => v)
    end
end