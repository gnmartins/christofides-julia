# christofides-julia
Approximate TSP solver using [Julia](https://julialang.org/) v0.5.2.

## Implementation
This solver implements the Christofides algorithm. It requires that the input graph follows the [TSPLIB](http://comopt.ifi.uni-heidelberg.de/software/TSPLIB95/) format.

## Usage
`./christofides-julia [-e] < input-graph`

If the `-e` flag is set, this solver will utilize the blossom5 software to calculate the minimum cost perfect matching. Otherwise, the matching is solved with a greedy algorithm.
